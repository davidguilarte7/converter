# Currency Converter - Laravel & VueJS Developer Test
## Pasos para la instalación después de clonar el repositorio
1. Crear una base de datos
2. Ingresar las credenciales de la base de datos en el archivo .env
3. Generar una clave de acceso en https://currencylayer.com/documentation
4. Agregar al archivo ```.env``` en ```CURRENCYLAYER_ACCESS_KEY="su_clave_de_acceso"```
5. ```composer install ```
6. ```npm install ```
7. ```php artisan key:generate ```
8. ```php artisan migrate --seed```
10. ```php artisan serve```
