<?php

use App\Models\ApiRequestLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('quota', function(){
    return Inertia::render('Error', ['status'=>429]);
});

Route::get('list', [\App\Http\Controllers\ConvertionController::class, 'list']);
Route::get('/', [\App\Http\Controllers\ConvertionController::class, 'index']);
Route::get('/home', [\App\Http\Controllers\ConvertionController::class, 'index']);
Route::middleware(['throttle:convert', 'log'])->get('convert', [\App\Http\Controllers\ConvertionController::class, 'convert']);
Route::get('logs', function(){
    $logs = ApiRequestLog::all();
    return Inertia::render('Logs', compact('logs'));
});
