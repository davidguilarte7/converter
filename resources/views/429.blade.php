<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Limite excedido</title>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    </head>
    <body>
        <div class="container" style="text-align: center";>
            <h1>Oops!</h1>
            <p>Parece que ha excedido su limite diario de 5 solicitudes<br> para usuarios no registrados</p>
            <a href="/register">Registrarse</a>
        </div>
    </body>
</html>
