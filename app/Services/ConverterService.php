<?php

namespace App\Services;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class ConverterService {
    protected $api;

    public function __construct(){
        $this->api = new Client(['base_uri' => 'http://api.currencylayer.com/']);
    }

    public function convert($from, $to, $amount){
        $query = [
            'from'       => $from,
            'to'         => $to,
            'amount'     => $amount,
            'access_key' => env('CURRENCYLAYER_ACCESS_KEY')
        ];

        try {
            $response = $this->api->get('convert', compact('query'));
            $response = json_decode($response->getBody(), true);
            if( $response['success'] && key_exists('result', $response) ){
                return $response['result'];
            }
        }
        catch( GuzzleException $e ){
            Storage::append('convertions.log', $e->getMessage());
            return '00.00';
        }
    }

    public function list(){
        $currencies = Cache::remember('currencies', 3600 * 12, function(){
            $query = ['access_key' => env('CURRENCYLAYER_ACCESS_KEY')];

            try {
                $response = $this->api->get('list', compact('query'));
                $response = json_decode($response->getBody(), true);
                if( $response['success'] && key_exists('currencies', $response) ){
                    $currencies = collect($response['currencies']);
                    return $currencies->map(fn($value, $key) => [
                        'label' => $key . " " . $value,
                        'value' => $key
                    ]);
                }
            }
            catch( GuzzleException $e ){
                Storage::append('convertions.log', $e->getMessage());
                return [];
            }
        });

        return $currencies;
    }

}
