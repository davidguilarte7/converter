<?php

namespace App\Http\Middleware;

use App\Models\ApiRequestLog;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LogApiRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $log = new ApiRequestLog();
        $log->url = $request->url();
        $log->method = $request->method();
        $log->controller = $request->route()->getAction('controller') ?: 'none';
        $log->action = $request->route()->getAction('as') ?: 'none';
        $log->parameters = json_encode($request->route()->parameters());
        $log->headers = json_encode($request->headers->all());
        $log->cookies = json_encode($request->cookies->all());
        // $log->session = json_encode($request->session()->all());
        $log->attachments = json_encode($request->allFiles());
        $log->input_data = json_encode($request->input());
        $log->client_ip = $request->ip();
        $log->user_agent = $request->userAgent();
        $log->save();
        return $next($request);
    }
}
