<?php

namespace App\Http\Controllers;

use App\Services\ConverterService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ConvertionController extends Controller
{
    protected $converter;

    public function __construct(ConverterService $converter)
    {
        $this->converter = new $converter();
    }

    public function list(){
        return $this->converter->list();
    }

    public function index(Request $request){
        $currencies = $this->converter->list();
        return Inertia::render('Converter', compact('currencies'));
    }

    public function convert(Request $request){
        $result = [
            'amount' => $request->amount,
            'from'   => $request->from,
            'to'     => $request->to,
            'output' => $this->converter->convert($request->from, $request->to, $request->amount)
        ];
        sleep(1);
        $currencies = $this->converter->list();
        return Inertia::render('Converter', compact('result', 'currencies'));
    }
}
