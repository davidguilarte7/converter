<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('api_request_logs', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('method');
            $table->string('controller')->nullable();
            $table->string('action')->nullable();
            $table->text('parameters')->nullable();
            $table->text('headers')->nullable();
            $table->text('cookies')->nullable();
            $table->text('session')->nullable();
            $table->text('attachments')->nullable();
            $table->text('input_data')->nullable();
            $table->ipAddress('client_ip');
            $table->string('user_agent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('api_request_logs');
    }
};
